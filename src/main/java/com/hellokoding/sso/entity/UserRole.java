package com.hellokoding.sso.entity;

public enum UserRole {
    ADMIN, MANAGER, USER
}
