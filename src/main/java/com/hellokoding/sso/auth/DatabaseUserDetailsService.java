package com.hellokoding.sso.auth;//package academy.softserve.aura.core.security;


import com.hellokoding.sso.entity.User;
import com.hellokoding.sso.entity.UserRole;
import com.hellokoding.sso.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;

import javax.persistence.NoResultException;
import java.util.ArrayList;


@Service
public class DatabaseUserDetailsService implements UserDetailsService {

    private final UserRepository userRepository;

    private Logger dudsLogger = LoggerFactory.getLogger("DatabaseUserDetailsService logger");

    @Autowired
    public DatabaseUserDetailsService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }


    @Override
    public UserCredentialsInfo loadUserByUsername(String login) {
        User fromDao = getUser(login);

        if (fromDao == null) {
            throw new IllegalArgumentException("Invalid login " + login);
        }

        dudsLogger.info("from dao user pass " + fromDao.getPassword());
        dudsLogger.info("in user details service");

       return new UserCredentialsInfo(login, fromDao.getPassword(), fromDao.getUserRole());
    }

    private User getUser(String login) {
        try {
            return userRepository.findByLogin(login);
        } catch (NoResultException e) {
            throw new IllegalArgumentException(login, e);
        }
    }

}
