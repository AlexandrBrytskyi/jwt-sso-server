package com.hellokoding.sso.auth;

/**
 * Created by obryttc on 24.07.2017.
 */
public interface UserDetailsService {

    UserCredentialsInfo loadUserByUsername(String userName);

}
