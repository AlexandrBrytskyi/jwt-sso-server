package com.hellokoding.sso.auth;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

@Controller
public class LoginController {
    private static final String jwtTokenCookieName = "JWT-TOKEN";
    private static final String signingKey = "signingKey";

    @Autowired
    private UserDetailsService userDetailsService;

    @Bean
    PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Autowired
    private PasswordEncoder passwordEncoder;

    @RequestMapping("/")
    public String home() {
        return "redirect:/login";
    }

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String login() {
        return "login";
    }

    @RequestMapping(value = "login", method = RequestMethod.POST, consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    public String login(
            HttpServletResponse httpServletResponse,
            @RequestParam("username") String username,
            @RequestParam("password") String password,
            String redirect, Model model) {
        if (username.isEmpty() || password.isEmpty() ) {
            model.addAttribute("error", "MissingParameters!");
            return "login";
        }


        UserCredentialsInfo userCredentialsInfo;
        try {
            userCredentialsInfo = userDetailsService.loadUserByUsername(username);
            if (!passwordEncoder.matches(password,userCredentialsInfo.getPassword()))
                throw new BadCredentialsException("Wrong pass");
        } catch (RuntimeException e) {
            e.printStackTrace();
            model.addAttribute("error", e.getLocalizedMessage());
            return "login";
        }

        String token = JwtUtil.generateToken(signingKey, username, userCredentialsInfo);
        CookieUtil.create(httpServletResponse, jwtTokenCookieName, token, false, -1, "localhost");

        return "redirect:" + redirect;
    }
}
