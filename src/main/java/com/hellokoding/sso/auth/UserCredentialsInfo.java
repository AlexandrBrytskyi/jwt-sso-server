package com.hellokoding.sso.auth;


import com.hellokoding.sso.entity.UserRole;

public class UserCredentialsInfo {

    private String userName;
    private String password;
    private UserRole role;

    public UserCredentialsInfo() {
    }

    public UserCredentialsInfo(String userName, String password, UserRole role) {
        this.userName = userName;
        this.password = password;
        this.role = role;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public UserRole getRole() {
        return role;
    }

    public void setRole(UserRole role) {
        this.role = role;
    }
}
