package com.hellokoding.sso.auth;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.impl.DefaultClaims;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.security.core.userdetails.UserDetails;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.HashMap;

public class JwtUtil {

    private static ObjectMapper mapper = new MappingJackson2HttpMessageConverter().getObjectMapper();

    public static String generateToken(String signingKey, String subject, final UserCredentialsInfo userDetails) {
        long nowMillis = System.currentTimeMillis();
        Date now = new Date(nowMillis);

        JwtBuilder builder = Jwts.builder()
                .setSubject(subject)
                .setClaims(new HashMap<String, Object>() {{
                    try {
                        put("userDetails", mapper.writeValueAsString(userDetails));
                    } catch (JsonProcessingException e) {
                        e.printStackTrace();
                    }
                }})
                .setIssuedAt(now)
                .signWith(SignatureAlgorithm.HS256, signingKey);
        return builder.compact();
    }

    public static String getSubject(HttpServletRequest httpServletRequest, String jwtTokenCookieName, String signingKey) {
        String token = CookieUtil.getValue(httpServletRequest, jwtTokenCookieName);
        if (token == null) return null;
        return Jwts.parser().setSigningKey(signingKey).parseClaimsJws(token).getBody().getSubject();
    }
}
